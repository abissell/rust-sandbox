extern crate debug;

fn main() {
    let hi = "hi";
    let mut count = 0i;

    while count < 10 {
        println!("count is {}", count);
        count += 1;
    }

    static MONSTER_FACTOR: f64 = 57.8;
    let _monster_size = MONSTER_FACTOR + 10.0;
    let _monster_size: int = 50;

    println!("MONSTER_FACTOR={}, monster_size={}",
             MONSTER_FACTOR, _monster_size);

    let item = "muffin";

    let price: f64 =
        if item == "salad" {
            3.50
        } else if item == "muffin" {
            2.25
        } else {
            2.00
        };

    println!("item={}, price={}", item, price);

    let four = 4;
    let five = 5;
    println!("is_four({})={}, is_four({})={}",
             four, is_four(four), five, is_four(five));

    let x: f64 = 4.0;
    let y: uint = x as uint;
    assert!(y == 4u);

    // `{}` will print the "default format" of a type
    println!("{} is {}", "the answer", 43i);

    // `{:?}` will conveniently print any type,
    // but requires the `debug` crate to be linked in
    println!("what is this thing: {:?}", "a thing");
}

fn is_four(x: int) -> bool {
    // No need for a return statement. The result of the expression
    // is used as the return value.
    x == 4
}
