use std::f64;
fn main() {
    if false {
        println!("that's odd");
    } else if true {
        println!("right!");
    } else {
        println!("neither true nor false");
    }

    println!("{}, {}, {}", signum(-10), signum(0), signum(1));

    let my_num = 1i;
    match my_num {
        0     => println!("zero"),
        1 | 2 => println!("one or two"),
        3..10 => println!("three to ten"),
        _     => println!("something else")
    }

    println!("angle(0.0, -0.6)={}", angle((0.0, 0.6)));
    println!("angle(0.35, 0.2)={}", angle((0.35, 0.2)));

    let age = 18i;
    match age {
        a @ 0..20 => println!("{} years old", a),
        _ => println!("older than 21")
    }

    let (a,b) = get_tuple_of_two_ints();
    println!("a={}, b={}", a, b);
}

fn signum(x: int) -> int {
    if x < 0 { -1 }
    else if x > 0 { 1 }
    else { 0 }
}

fn angle(vector: (f64, f64)) -> f64 {
    let pi = f64::consts::PI;
    match vector {
        (0.0, y) if y < 0.0 => 1.5 * pi,
        (0.0, _) => 0.5 * pi,
        (x, y) => (y / x).atan()
    }
}

fn get_tuple_of_two_ints() -> (i32, i32) {
    (58i32, 20i32)
}
